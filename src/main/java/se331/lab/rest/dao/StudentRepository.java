package se331.lab.rest.dao;

import org.springframework.data.repository.CrudRepository;
import se331.lab.rest.entity.Student;
import java.util.List;

public interface StudentRepository extends CrudRepository<Student, Long> {
    List<Student> findAll();
}
