package se331.lab.rest.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import se331.lab.rest.dao.StudentRepository;
import se331.lab.rest.entity.Student;


@Component
public class DataLoader implements ApplicationRunner {
    @Autowired
    StudentRepository studentRepository;
    @Override
    public void run(ApplicationArguments args) throws Exception {
        studentRepository.save(Student.builder()
                .studentId("SE-001")
                .name("Prayuth")
                .surname("The minister")
                .gpa(3.59)
                .image("http://13.250.41.39:8190/images/tu.jpg")
                .penAmount(15)
                .description("The great man ever!!!!")
                .build());
        studentRepository.save(Student.builder()
                .studentId("SE-002")
                .name("Cherprang ")
                .surname("BNK48")
                .gpa(4.01)
                .image("http://13.250.41.39:8190/images/cherprang.png")
                .penAmount(2)
                .description("Code for Thailand")
                .build());
        studentRepository.save(Student.builder()
                .studentId("SE-003")
                .name("Nobi")
                .surname("Nobita")
                .gpa(1.77)
                .image("http://13.250.41.39:8190/images/nobita.gif")
                .penAmount(0)
                .description("Welcome to Olympic")
                .build());
    }
}
